#!/bin/bash

# Figure out where the root folder of this script is on the users computer...
current_dir=$(pwd)

# Ask user what they want to call this instance of the script...
echo "What would you like to call this backup instance?"
read -r project_name

# Check name does not already exists...
while [ -d ~/bin/"$project_name" ];
  do echo "That name already exists, please choose another" && read -r project_name
done

# Get paths for rsync...
echo "What is the full path for your pendrive..."
read -r pen

echo "And what is the full path to your local directory we will be syncing with?..."
read -r local

# Define the variable 'remote' - the full path to the directory on the pendrive...
remote=/$pen/$project_name

# Check for the 'bin' dir in the users 'Home' directory and create it if not present...
if [ -d "$HOME"/bin ];
  then echo "$HOME/bin exists, nothing to do..."
    else mkdir "$HOME"/bin
fi

# Finally copy the project directory over to the final place - /home/user/bin/...
cp -r "$current_dir" "$HOME"/bin

# Rename the project directory...
mv "$HOME"/bin/USB_pendrive_backup "$HOME"/bin/"$project_name"

# Set new paths in main.sh...
sed -i "s|/pendrive/path/|${pen}|" "$HOME"/bin/"$project_name"/main.sh
sed -i "s|/local/dir/path/|${local}/|" "$HOME"/bin/"$project_name"/main.sh
sed -i "s|/full_path_to_project_dir|${remote}|" "$HOME"/bin/"$project_name"/main.sh

# Edit menu entry...
sed -i "s/name/${project_name}/" "$HOME"/bin/"$project_name"/menu.desktop
sed -i "s|path|$HOME/bin/${project_name}/main.sh|" "$HOME"/bin/"$project_name"/menu.desktop
sed -i "s|comment|${project_name} script|" "$HOME"/bin/"$project_name"/menu.desktop
sed -i "s/terminal/true/" "$HOME"/bin/"$project_name"/menu.desktop
sed -i "s|icon|$HOME/bin/${project_name}/icon.svg|" "$HOME"/bin/"$project_name"/menu.desktop

# Copy menu file...
cp "$HOME"/bin/"$project_name"/menu.desktop "$HOME"/.local/share/applications

# Rename menu file...
mv "$HOME"/.local/share/applications/menu.desktop "$HOME"/.local/share/applications/"$project_name".desktop

# Change 'announce' name (the name that confirms which script the user is running when they run the script)...
sed -i "s|announce|$project_name|" "$HOME"/bin/"$project_name"/main.sh

# Clean up...
rm "$HOME"/bin/"$project_name"/install.sh
rm -rf "$HOME"/bin/"$project_name"/.idea
rm "$HOME"/bin/"$project_name"/menu.desktop

# Uninstall script data...
sed -i "s/project-name/$project_name/" "$HOME"/bin/"$project_name"/uninstall.sh

# Let the user know we're done!
echo "
      All done! Press 'Enter' to close this window"
read -r
