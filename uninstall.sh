#!/bin/bash
# Script to uninstall Pendrive Backup
# 2023-06-04 v1.0

# Config...
project_name=project-name

# Confirm the user really knows what they are about to do and really wants to do it...
echo "
        You are about to remove $project_name from your computer.

              Are you sure you want to do this? 'y' or 'n'..."
read -r confirm

until [ "$confirm" = y ] || [ "$confirm" = n ];
  do echo "Invalid input. You must answer with a 'y' or 'n'. Please try again..." && read -r confirm
done

if [ "$confirm" = n ];
  then echo "
           Aborting the Uninstallation!
        Press 'Enter' to close this window..." && read -r && exit
    else echo "Uninstalling..."
fi

# Remove menu entry...
rm ~/.local/share/applications/"$project_name.desktop"

# Change dir to enable uninstallation...
cd ~/ || exit

# Remove root dir...
rm -rf ~/bin/$project_name

echo "This instance of Pendrive Backup $project_name has been removed"
echo "Please press 'Enter to close this window..."
read -r