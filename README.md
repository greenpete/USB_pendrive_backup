# Pendrive Backup

If you would rather use the 'sneaker net' to synchronise data between computers, then this script might be for you. Assuming you use Linux...

'Pendrive Backup' is a script for Linux systems, that can be installed multiple times for different back up jobs.
The idea being that if you are working on a project and want to switch between computers, say desktop at the office and laptop at home, this script makes it easy to transfer the relevant data back and forth between computers.

When you run an (installed) instance, just select 'to' or 'from' the USB device, ('t' or 'f' in reality).

### Installation

After downloading the script archive and decompressing it, run the install.sh script and answer the questions within.
This will install the 'main' script and icon to your home directory, under ~/bin.
It will also install a menu entry which will be under, 'Other'.

You can do this as many times as you want for as many projects as you have, just keep the initial install package to run from each time, (or re-download it from here).

### Uninstalling

You can also uninstall this app using the uninstall.sh script found at ~/bin/{project name} (where project name is whatever you named it when installing).
The uninstall script found in the 'installed' location is specific to that instance.
Running the uninstall.sh file in the initial, decompressed archive will do nothing.

### Usage

Just click on the link in your menu, (Menu > Other > {project_name}) and follow the prompts.

### Contributing

Please feel free to raise an issue or submit a pull request if you think this script could be improved.

### Credits

The dev's that wrote rsync, sed and bash.

### The developer

That'll be me I guess, Peter Green though calling it development seems to be pushing it a bit for a bit of bash scripting! It's the nearest I have got though ;-)
https://greenpete.co.uk/

### License

MIT