#!/bin/bash

###############################################
#                                             #
# Script for backing up projects via pendrive #
# By Peter Green                              #
# Version 0.2                                 #
# Date of first version; 2022-09-03           #
# Date of last update; 2023-01-22             #
# Moved to my own Gitea instance 2022-12-27   #
#                                             #
###############################################

# Vars...
# Set backup dir paths...
pen=/pendrive/path/
local=/local/dir/path/
remote=/full_path_to_project_dir # Full path to project dir on pendrive.

# Announce the name of the script...
echo "
                          ::announce::"

# Check to see if the pendrive is present...
if [ -d $pen ];
  then echo "
                   Pendrive found, proceeding..."
    else echo "
                    Pendrive not found, aborting!
                 Press 'Enter' to close this window..." && read -r && exit
fi

# Check to see if the dest dir on pendrive is present...
if [ -d remote ];
  then echo "Directory on pendrive found, proceeding..."
    else echo "Directory on pendrive not found, creating... " && mkdir $remote
fi

# Give the keyboard monkey the heads up...
echo "

                       ::IMPORTANT NOTE::

    This script backs up 'incrementally' so data will accumulate."

# Ask if we should send to or fetch from the pendrive...
echo "

          Transfer 'to' or 'from' pen drive? ('t' or 'f')"

read -r direction

# Let the muppet know what we are doing...
echo "
           You chose $direction. Validating input..."

# Check that the duffus behind the keyboard gets it right...
until [ "$direction" = t ] || [ "$direction" = f ];
  do echo "
           Invalid input. You must answer 't' or 'f'.
                   Please try again..." && read -r direction
done

# Run the backup...
if [ "$direction" = t ];
    then rsync -atv --progress $local $remote
        else rsync -atv --progress $remote/ $local
fi

# Let the mouse molester know we are finished and get some engagement from them...
echo "
     #########################################################
     #                                                       #
     # Backup finished, Press 'Enter' to close this window.  #
     #                                                       #
     #########################################################"

read -r
